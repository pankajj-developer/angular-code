import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { ViewTextComponent } from './text/view-text/view-text.component';

const routes: Routes = [
  {
    path : '',
    component : AppComponent,
    children : [
      {
        path : '',
        component : ViewTextComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
