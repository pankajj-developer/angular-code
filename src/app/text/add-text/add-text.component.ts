import { Component, OnDestroy, OnInit, TemplateRef } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { ToastrManager } from 'ng6-toastr-notifications';
import { TextService } from '../text.service';

@Component({
  selector: 'add-text',
  templateUrl: './add-text.component.html',
  styleUrls: ['./add-text.component.css'],
})
export class AddTextComponent implements OnInit, OnDestroy {

  addTextForm: FormGroup = new FormGroup({});
  textFieldFocused = false;

  constructor(private formbuilder: FormBuilder, private textService: TextService, private Toastr : ToastrManager) {}

  public ngOnInit(): void {
    this.initializeForm();
    this.textService.getResetAddFormEvent().subscribe(
      (eventValue) => {
        if(eventValue) {
          this.resetAddForm();
        }
      }
    )
  }

  public ngOnDestroy(): void {
    this.textService.getResetAddFormEvent().unsubscribe();
  }

  private initializeForm(): void {
    this.addTextForm = this.formbuilder.group({
      textFormControl: ['', [Validators.required, Validators.maxLength(140), this.patternValidator()]],
    });
  }

  public get f() {
    return this.addTextForm.controls;
  }

  public addText(): void {
    if (this.addTextForm.invalid) return;

    this.textService
      .saveText({ text: this.f.textFormControl.value })
      .subscribe({
        next: (data) => {
          //Emit event to List Text Screen to refresh list
          this.textService.getRefreshListPageEvent().emit(true);
          this.Toastr.successToastr('User name added successfully','Success');
        },
        error: (error) => {
          this.Toastr.errorToastr(error.error.errorMessage,'Error');
        },
      });
      this.resetAddForm();
  }

  private patternValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value) {
        return null;
      }
      const regex = new RegExp('[<>/:&+%;\"]|\\.\\w{2,4}');
      const valid = !regex.test(control.value);
      return valid ? null : { invalidText: true };
    };
  }

  private resetAddForm(): void {
    this.addTextForm.markAsPristine();
    this.addTextForm.reset();
    this.addTextForm.updateValueAndValidity();
  }
}
