import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { Text } from './text';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
  })
export class TextService {

  private refreshListPageEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
  private resetAddFormEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private http: HttpClient) { }

  getAllTexts(): Observable<Text[]> {
    return this.http.get<Text[]>('/api/assignment/user-text/find-all-user-text');
  }

  saveText(text:Text): Observable<any> {
    return this.http.post<any>('/api/assignment/user-text/save', text);
  }

  deleteText(text:Text): Observable<any> {
    return this.http.delete<any>('/api/assignment/user-text/delete/'+text.id);
  }

  public getRefreshListPageEvent(): EventEmitter<boolean> {
    return this.refreshListPageEvent;
  }

  public getResetAddFormEvent(): EventEmitter<boolean> {
    return this.resetAddFormEvent;
  }
}