import { Component, OnDestroy, OnInit, TemplateRef } from '@angular/core';
import { ToastrManager } from 'ng6-toastr-notifications';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Text } from '../text';
import { TextService } from '../text.service';

@Component({
  selector: 'list-text',
  templateUrl: './list-text.component.html',
  styleUrls: ['./list-text.component.css'],
})
export class ListTextComponent implements OnInit, OnDestroy {

  textList: Text[] = [];
  modalRef : BsModalRef;
  currentIndex : number = -1;
  showLength : number = 5;

  constructor(private textService: TextService,
    private modalService: BsModalService,
    private Toastr : ToastrManager) {
      this.textService.getRefreshListPageEvent().subscribe(
        (eventValue) => {
          if(eventValue) {
            this.updateTextList();
          }
        }
      )
    }

  public ngOnInit(): void {
    this.textService.getAllTexts().subscribe((data) => {
      this.textList = data;
    });
  }

  public ngOnDestroy(): void {
    this.textService.getRefreshListPageEvent().unsubscribe();
  }
  
  public openConfirmPopup(template : TemplateRef<any>,index: number): void {
    this.currentIndex = index;
    this.modalRef = this.modalService.show(template, {
      class: "modal-dialog-centered deleteAcademicConfirm themeModal",
    });
  }

  public deleteText(text: Text): void {
    if(this.modalRef){
      this.modalRef.hide()
    }
    this.textService.getResetAddFormEvent().emit(true);
    this.textService.deleteText(text).subscribe({
      next: (data) => {
        this.updateTextList();
        this.Toastr.successToastr('Text removed successfully','Success');
      },
      error: (error) => {
        this.Toastr.successToastr(error.message,'Error');
      },
    });
  }

  public getDeleteConfirmMessage(): string {
    return this.textList.length > 0 ? ('Are you sure you want to delete ' + this.textList[this.currentIndex].text + ' ?') : '';
  }

  private updateTextList(): void {
    this.textService.getAllTexts().subscribe((data: Text[]) => {
      this.textList = data;
    });
  }

  public showAll(): void {
    if(this.showLength === 5){
      this.showLength = this.textList.length
      return
    }

    if(this.showLength === this.textList.length){
      this.showLength = 5;
      return
    }
  }
}
