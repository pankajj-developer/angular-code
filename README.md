# User Text Management

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.1.2.


## Running Application on local environment

1. Clone repository
2. Run below command to download all the project dependencies - 
	npm install
3. Run below command to start the server
	npm start
4. Navigate to `http://localhost:4200/`

## Proxy for back-end integration

update the proxy.config.json file to change the backend server details - 

 {
    "/api/*": {
      "target": "http://localhost:9000",
      "secure": false,
      "pathRewrite": {
        "^/api": ""
      }
    }
  }
 